# ingredents
- Bacon
- mexican chorizo
- chilies
- beans
	- pinto
	- black beans
- batleaf
- fresh coriander

# method 
1. Make [black beans](../../../food/cooking/recipes/black%20beans.md)
2. Saute bacon, onion & chorizo. Season with garlic and pepper. Then add chiles.
3. Add cooked beans along with gravey and reduce to desired thcikness.
4. for the last 10 mins of cooking add the corrianer.
5. Add chicharrones for garnish.