# Ingredients

* Ham(Gammon) joint
* 2l Cola
* Garlic
* Onion
* Brown sugar and course salt

# Method

* Brine the joint overnight
* Put joint in cold water and bring up to simmer (this removes some scum from the joint)
* Remove the joint from the water and get rid of it.
* Clean the joint and pot.
* Put the cola, onion and garlic in the pan along with the joint and bring to a simmer for an hour.
* Preheat the oven to 230C (450F)
* Remove joint from the pan remove the skin leaving as much fat as possible score the fat and sprinkle the sugar and salt on top.
* Bring the cola mixture to a boil so it can reduce (this works faster if you transfer it to something with a larger surface area eg a frying pan)
* let the joint rest for 10 mins then put in the oven for an hour basing with the reduced cola sauce every 15 mins.