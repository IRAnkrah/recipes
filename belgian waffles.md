# Ingredients
* 130g flour
* 7g baking powder
* 38g Sugar
* 2g salt
* 1 large egg Seperated
* 120ml Oil (114g)
* 240ml milk (249g)
* vanilla Extract

# Method
* Preheat waffle iron, spray with non stick
* In a large bowl mix the dry ingredents
* In seperate bowl beat egg white till stiff peaks
* In yet anther bowl mix the wet ingredents
* Add wet to dry and mix.
* Fold in the egg whites
* pour into waffle iron and cook for 7 mins
