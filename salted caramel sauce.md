# Ingredients
* 100% brown sugar
* 50% butter
* 50% milk/cream

# Suggested portion
* 100g brown suger
* 50g butter
* 50g milk/cream

# Method
* Melt the suger in a pan and get to desired colour
* Cut the heat and mix in the butter aggressivly
* Mix in the milk/cream a little at a time untill it's all in.
