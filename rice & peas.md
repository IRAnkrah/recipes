# Ingredients
* 100% basmati rice
* 60% dried kidney beans or 100% tinned kidney beans
* 100% coconut milk
* 100% water

# Suggested portion
* 250ml rice
* 150g beans
* 250ml coconut milk
* 250ml water

# Method
*Made in an instant pot pressure cooker*

* Soak the beans overnight.
* Discard the soaking water rince off the beans and put in the pressure cooker with the water and coconut milk and set to cook at high pressure for 15 mins.
* Wash the rice you animal.
* When the timer is up use the quick release, mix in the rice into the beans and liquid and set to rice cook for 17 mins.
* When the timer is up let slow release for 15 mins, stir and serve.


# Notes
* Try half the water.
