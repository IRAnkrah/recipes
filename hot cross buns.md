[base recipe](https://www.recipetineats.com/hot-cross-buns-recipe/)

*try reducing the recipe by 20% next time to make smaller buns*
# Ingredients 
## 12
### buns
- 640g flour
	- 440g bread flour
	- 200g whole weat flour
- 375ml milk
- 210g rasins
- 110g sugar
- 50g butter
- 1 egg
- 9g yeast
- 2 tsp cinimon
- 0.5tsp salt
- 2 orange zest
### crosses
- 75g white flour
- 70g water
### glaze
- 1 tbsp apricote jam
- 2 tbsp water

# method
1. Bloom yeast in warm milk and 2 tsp of sugar
2. Place flour, yeast, sugar, all spice, cinnamon, and salt in a large bowl. Briefly mix with stand mixer fitted with a dough hook.
3. Add butter, milk, egg, sultanas and zest.
4. Standmixer: Mix until a smooth elastic dough forms - 5 minutes on Speed 2 of standmixer. After 1 minute, add extra flour if required, just enough so dough comes away from side of bowl when mixing and doesn't stick terribly to your fingers.
5. cover dough and leave to rise till doubled (1hr)
6. prepare cooking vessel  (9x13 hight sided tray or cast iron)
7. punch out dough and divide into 12 peices.
8. shape peices creating tension
9. preheat oven to 180c (350f)
10. cover and rise till 75% bigger  (30 mins)
11. make cross mix and put on buns immdeailrty before going in the oven
12. bake for 22 mins
13. mix glaze and warm in micowave to brush on immedialy after they leave the oven

# notes
- only one batch can fix in the stand mixer at once.

