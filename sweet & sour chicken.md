# Ingredients
* 250g Chicken
* 1tsp Cornstarch
* Salt
* Pepper
* 1/2 Onion
* 1/2 Bell Pepper
* 1/3 tin Pineapple chunks

# Sauce
* 50g Tomato Ketchup
* 25g Apple cider vinegar *Can sub in white vinegar*
* 25g Pineapple juice
* 15g Sugar
* tbsp Soy sauce
* tsp Worcestershire sauce

# Method
* Season the chicken with salt, pepper and cornstarch and leave to marinade.
* Sear the chicken in a hot pan till almost cooked through, then remove from the pan and leave to one side.
* Fry the onion and bell pepper until starting to go soft, then add the pineapple and fry for another few mins.
* Mix all the sauce ingredients together then pour into the pan and simmer to reduce by 1/3.
* stir the chicken back and continue to simmer all is up to temp and the desired thickness has been reached.