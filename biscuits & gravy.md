# Biscuits
* 250g all purpose flour
* 14g baking powder
* 14g Tablespoon granulated sugar
* 7g teaspoon salt
* 85g frozen grated butter.
* 177ml whole milk

## Method
preheat oven to 220c (425f)

mix butter into the dry ingredients, combine with the milk , turn out onto a well floured surface and laminate dough to bring together (fold in on itself and flatten out over and over). when combines flatten out into a 2.5 cm sheet that you can cut into 6 pieces (also cut the edges to get an even rise) then place on a parchment lined baking sheet and oven for 15mins.

# Country Gravy
* 450g Sausage meat
* 32g flour
* 590ml milk
* one onion finely diced

## Method
fry off sausage till cooked add onion half way through, sprinkle over and mix in flour while cooking (half at a time) then add the milk a little at a time to create a thick sauce.

# Reference
- https://sugarspunrun.com/easy-homemade-biscuits