# Ingredients
* 100% milk
* 50% flour
* 5% melted butter
* 3% sugar
* 1 egg per 140g

# Suggested Portion: 4
* 140g milk
* 70g flour
* 7g melted butter
* 4g sugar
* 1 egg

# Method
* Mix everything then add the butter.
* Preheat a pan on medium
* cook.
