# ingredents
- 500g bead flour
- 300g water
- 60g sofened butter
- 1 egg 
- 7g salt
- 7g yeast
- 7g sugar

# method
- mix flour and salt in the standmixer
- add suger and yeast to the warmed water
- mix in an egg once the yeast has done it's thing.
- add the wet to the dry and mix untill it comes together
- once the dough looks smooth add the butter 1/4 at a time untill fully incorperated.
- allow to rise till doubled in size (~1 hour)
- punch down, and shape into 8 100g balls
- flatten them a little, place in the baking vessal and allow to rise again untill 75% bigger
- spray with water and place in an overn at 220c (400f) for 25mins

# notes
- try adding 80g of oats that have been softened in just enouhg water to do so at the end of the mixing.
# reference
- [BBC easy bread roll](https://www.bbcgoodfood.com/recipes/easy-bread-rolls)