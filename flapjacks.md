# Ingredients
* 100% rolled oats
* 75% butter
* 75% brown sugar
* 45% maple syrup *honey can be substituted*
* 50% mix-in *Options*
  * raisins
  * chocolate
  * nuts

# For an 8" Square pan
* 200g rolled oats
* 150g butter
* 150g brown sugar
* 90g maple syrup
* 100g raisins

# For an 8x16 pan
* 450g rolled oats
* 340g butter
* 340g brown sugar
* 200g maple syrup
* 225g mix-in

# Method
* preheat the oven to 180C.
* cook the butter, sugar and maple syrup over a medium low heat stirring continuously until it's fully dissolved together.
* add in the vanilla and combine.
* mix in the oats making sure they are thoroughly combined, then mix in the raisins.
* transfer to your baking vessel and put in the oven and bake for 15 minutes.
* once removed it will still look quite liquidy, allow to cool completely (2-3 hours).
* removing from the baking vessel, cut into portions and store in an air-tight container in the fridge.
