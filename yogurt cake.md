# origianl ingredients
* 230g ap flour
* 170g oil (canola/olive) *or 100g oil + 10g butter
* 150g sugar
* 150g yogurt
* 3 eggs
* 6g baking poweder
* 5g vanilla extract

# modified ingredients 
* 230g ap flour
* 100g oil
* 150g sugar
* 150g yogurt
* 2 eggs
* 10g vanilla extract
* 6g baking poweder

# modified ratios
* 100% flour
* 74% oil
* 65% sugar
* 65% yogurt
* 4% vanilla extract
* 3% baking powder

# method
* mix wet ingredients till fluffy
* fold in dry ingredents
* bake for 50 mins at 375f
