# Ingrediants
* 600g lean/extra lean mince
* 1 can of Tomato (I prefer whole tomatos)
* 1 tin tomato puree (concentrated tomato)
* 1 big carrot (2 regual carrot)
* 1 onion
* 200g mushrooms
* 4 cloves of garlic
* 2 tablespoon Soy sauce
* 1 tablespoon worschershire sauce
* 1 teaspoon oregano
* 3 bayleafs
* 2 tablespoon paprika

[optional]
* 200ml stock/good beer/red wine
* rind of hard itlalian cheese
* 40g of dark chocloate (the darker the better)

# Method
* preheat a heavy bottom pan (with a lid) on medium high
* while it's preheating, grate the carrot on the smallest holes and thinly slice the onion and garlic.
* when the pan is hot press the mince into the pan (like a massive burger) to allow it to sear
* after 3-4 mins turn the meat over (it don't matter if it breaks up) to brown the other side for 1 minute.
* break the meat up and remove from the pan while leaving the fat behind.
* turn the heat down to medium then saute the carrot for a min then add the onions and garlic and saute till soft.
* add a little more fat if the pan is looking dry, then add the spices and mix consstantly untill fragrant.
* add the tin of tomato puree and stir into the the rest of the things an cook through for a few mins.
* [optional] if you have a good stock/ beer or red wine to hand add 200ml in to deglaze the pan and cook off the alcohol for a minute.
* add the meat back into the pan then the tin of tomatos (if they are not crushed crush them on your hands this is what I prefer to do)
* bring up to heat then add the bay leaf soy and worschershire sauce.
* salt to taste
* [optional] if you have the rind of a hard itallian cheese add it to the sauce (you will have to remember to find and remove it before serving)
* add lid to the pan and simmer it gently for about 40 mins stiring occasionaly
* chop the mushrooms into 8ths add to the pan and continue to simmer
* after 10 mins remove the lid  taste and adjust seasonsing. if it's too tart/bitter adding a table spoon of sugar can help.
* [optional] add 40g of the darkest choclocate you can find a litte at a time and stiring in.
* leave the lid off and allow the sauce to reduce (turn down if nessisary)
