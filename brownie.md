# Ingredients 9x13 sheet pan
* 500g sugar _(try 400g next time)_
    * 250g white
    * 250g brown
* 290g butter
* 160g unsweatended cocoa powder
* 130g flour
* 1 tbpn vanillia extract
* 1/4 tssp salt
* 4 eggs
* _optional_ 150g mixin

# Method
* preheat oven to 325F with sheet pan and parchemnt paper in
* put butter, sugar, cocoa powder and salt in a pan and heat under low temp and mix constandly (it will be gritty)
* take off the heat and put in stand mixer with the padle attachemnt.
* add the vinilla and the egg one at a time and combine
* once the eggs are fully combined mix in the flour and don't worry about overworking the mix.
* spread in the baking sheet and cook for 20-30 mins untill cooked through
* let cool completly then remove from sheet pan and cunt into smaller peices than you think

# reference
* https://www.inspiredtaste.net/24412/cocoa-brownies-recipe/