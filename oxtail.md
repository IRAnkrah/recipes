# Ingredents
* 1kg oxtail
* 50g brown sugar
* 16g (1tbsp) soy sauce
* 17g (1tbsp) worcester sauce
* 7g (2tsp) garlic powder
* 7g (1tsp) allspice
* 10g (1tsp) browning
* yellow onion
* 3 stalks of celery
* 4 green onions
* 5 garlic cloves
* 2 carrots
* 1 scotch bonnet pepper
* 250ml beef broth *you can probably sub this with water in a pinch*
* 15g (1 tbsp) ketchup
* 1 tbsp dried thyme
* 2 tbsp water
* 1 tbsp cornstarch
* 125g raw butter beans (250g cooked)

# Method
## Pressure cooker
* Marinade the oxtail with the brown sugar, soy sauce, Worcestershire sauce, salt, garlic powder, black pepper, all-spice, browning 30ml broth.
* Set Pressure Cooker on High Saute and sear the oxtail to build fond.
* Remove oxtail after browning and place in bowl and deglaze with 2 Tbsp of beef broth
* Add the yellow onions, green onions, carrots, garlic, and celery. Stir and fry until the onions and celery have softened.
* Stab the scotch bonnet pepper a few times to create slits.
* Mix the leftover marinade, beef broth, ketchup and scotch bonnet pepper to the pot along with some thyme.
* Add the Oxtail back, you want the liquid to come up to about 2/3 the larger pieces.
* Cook on high pressure for 45 minutes, then allow for natural release (20-30 mins).

  *At this point I move the stew to a different pot so that I can use my instant pot to make the rice.*
* Remove the solids from the pot and set to simmer. Once liquid begins to simmer, create a corn starch slurry in a separate bowl and stir into the pot a little at a time. Once fully incorporated add the butter beans and continue to stir.
* After about 5 mins add the previously separated solids (oxtails and vegetables) back to the pot and simmer low and stirring occasionally for at least 5 mins and until you are ready to serve.

# Reference

* <https://www.myforkinglife.com/jamaican-oxtails/>
* <https://cooking.nytimes.com/recipes/1013460-jamaican-oxtail-stew>