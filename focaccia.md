# Ingredients
100% Flour
80% Water
10% olive oil
2.5% suger
2.5% salt
1% yeast

# Serving 1 quter pan thick
500g flour
400g water
50g olive oil
12.5g salt
12.5g sugaur
5g yeast

# Method
* Bloom Yeast in the warm water and suger
* Mix flour and salt then add the oil.
* Mix the yeast and the dry ingretents and combine untill no dry remians
* Cover air tight and leave to rest for a few hours.
* Fold and put in the fridge over night
* Oil baking vessel then turn dough out into it to come up to room temp and to rise.
* Preheat oven to higest temp for 1 hour.
* once dough is rested, coax it to the edges.
* put toppings on and put in oven for 12-15 mins.
* if bottom is not crispt enough cook on stove for a few mins.
