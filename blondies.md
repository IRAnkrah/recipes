# Ingredents
## 8" pan
* 220g dark brown sugar
* 110g butter
* 1 large egg
* 1 tbsp vanilla
* 130g all purpose flour
* 60g rough cut white chocolate
* 3.5g baking powder
* salt to taste

## 8x13 sheet pan
* 440g dark brown sugar
* 220g butter
* 2 large eggs
* 2tbsp vanilla
* 260g all purpose flour
* 7g baking powder
* 120g white chocolate (rough cut/chips)
* salt to taste.

# Method
* preheat oven to 175C
* melt the butter *if you are feeling brave browned butter is great*
* put the melted butter into a bowl with the sugar and mix aggressively until fully combined
* add the egg and vanilla to the batter and mix a little less aggressively until combined
* mix the flour, salt and baking poweder in a seperate bowl.
* fold into the batter 1/3 at a time.
* fold in the white chocolate and any other mixins you want
* put in baking vessal and into the oven for 28 mins. (when you take it out check its baked by inserting a toothpick/knife, it should pull out clean)
