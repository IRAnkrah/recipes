# Ingredients
* 100% cooked chickpeas
* 40% tahini
* 25% lemon juice
* 12% olive oil
* garlic
* salt
* papper

# Portion
* 250g chickpeas (dry chickpeas double in weight when cooked)
* 100g tahini
* 62g lemon juice
* 30g olive oil
* 3 cloves of garlic

# Method
* soak half required chickpeas in salted water overnight.
* drain and put in instant pot with enough water to cover twice, galic, slat and a bay leaf and cook for 17 mins and natural relase for 20 mins.
* mince garlic cloves and soak in lemon juice while the chickpeas are cooking (or at  least 20 mins)
* process the lemon and garlic with the tahini until it forms a paste.
* add the drained chickpeas and process with the rest of the ingredents until the the deisred consistancy is reached.
