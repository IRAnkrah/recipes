# Ingredients
* 2 tablespoons butter
* 1 leek
* 2 cloves of garlic
* 1 Jalapeno
* 3 ears of corn
* 2 bayleaf
* up to 500ml chicken stock

# Method
* Shuck the corn, harvest the kernels from the cob and cut the cobs in half.
* Finely dice the leek garlic and chili and cook in one table spoon of butter in a pressure cooker on saute until softened.
* Add the corn kernels, cobs and bayleaf to the pressure cooker and just cover with chicken stock.
* Cook at high pressure for 15 mins, then quick release.
* Remove the cobs and bayleaf.
* Transfer to a blender and blend, adding stock until the desired consistency is reached.
* Add salt and pepper to taste then blend in one table spoon of butter and serve

# Reference
<https://www.seriouseats.com/recipes/2015/08/pressure-cooker-corn-soup-recipe.html>
