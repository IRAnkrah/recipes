# Ingredients
* 100% flour
  * 55% whole wheat
  * 45% all purpose
* 85% water
* 2.5% yeast
* 2.5% sugar
* 3% salt

# Suggested portion:
* 300g flour
  * 165g whole wheat
  * 135g all purpose
* 9g salt
* 255g water
* 7.5g sugar
* 7.5g yeast

* 450g flour
  * 250g whole wheat
  * 200g all purpose
* 380g water
* 13g salt
* 11g sugar
* 11g yeast

# Method:
* put sugar into warm (38C) water and bloom yeast.
* mix salt and flour, once yeast is bloomed mix in the wet ingredients.
* fold the dough every few hours for 6 hours.
* place in fridge overnight shaped in a tea towl and bowl.
* preheat oven to 230C (450F) with dutch oven in for an hour
* flour the bottom of the dutch oven, place the dough in, splash with water and cook covered for 30 mins then take the lid off a cook uncovered for 20 min
* let rest for an hour
