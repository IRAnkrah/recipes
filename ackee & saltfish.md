# Ingredients
* 250g saltfish
* 2 onions
* 4 cloves of garlic
* 1 scotch bonnet pepper
* 1 bell pepper
* 1 tin of ackee (drained)

# Method
* soak the salt fish in hot water for an hour twice.
* fry the onions and garlic in untill translucent.
* add the peppers untill soft.
* add the fish and cook for 5 mins.
* add the ackee and cook for 2 mins.
* cut the heat, mix in pepper and paprika to taste and serve.

