# Ingredents
* 100% flour
  _optional_
  * 70% all purpose
  * 30% whole wheat
* 72% water
* 8% olive oil
* 2.5% yeast
* 2.5 sugar
* 0.8% salt

# for 8 pitas
* 330g flour
  * 231g all purpose
  * 99g whole wheat
* 237g warm water
* 27g olive oil
* 7.5g sugar
* 7.5g yeast
* 2.4g salt

# Method
* bloom the yeast with the water and sugar
* mix the flour and salt, then add the oil and yeast slury and mix untill a shaggy dough forms
* knead untill smooth and taught 7-10 mins
* transfer to a clean oiled blow and allow to rise untill double in size.
 preheat oven with backing steel to 250C (475F)
* punch down divide into portions roll into balls and let rests for antoher 10 mins.
* roll balls out untill about 3mm thick (will be about 20cm disc).
* place discs on the backing steel should puff and be ready to turn in 2 mins, turn and cook for another min then done.

