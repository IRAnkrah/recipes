# Ingredients
* 100% flour
* 64% water
* 13% sugar
* 12% butter (room temp)
* 5% baking powder
* 1.5% salt

# Suggested portion (6 small dumplings)
* 125g flour
* 80g water
* 16g sugar
* 15g butter
* 6g baking powder
* 1.5g salt

# Method
* sift butter, baking powder and salt into a food processor.
* add the room temperature butter and process.
* add the water a little at a time to combine, be sure to not over work.
* leave to rest.
* knead and seperate into 6 peices and roll into balls.
* heat oil to 180C (350F)
* fry of 10-15 mins.


