# Ingredients
100% Flour
80% Water
2.5% suger
2.5% salt
2.5% olive oil
1% yeast


# Serving 1 pizza
200g flour
160g water
5g sugar
5g salt
5g olive oil
2g yeast

# Method
* Bloom Yeast in the warm water and suger
* Mix flour and salt then add the oil.
* Mix the yeast and the dry ingretents and combine untill no dry remians
* Cover air tight and leave at room temp for 8-24 hours.
* Turn out and form into a tight ball
* Oil a cast iron pan and gently try and spread the dough in the pan (it probaly wont fil it yet)
* Cover air tight and leave to rest for 2 hours.
* Preheat oven to higest temp for 1 hour.
* once dough is rested, coax it to the edges.
* put toppings on and put in oven for 12-15 mins.
* if bottom is not crispt enough cook on stove for a few mins.

# ref
https://www.seriouseats.com/recipes/2013/01/foolproof-pan-pizza-recipe.html
https://slice.seriouseats.com/2012/07/the-pizza-lab-three-doughs-to-know.html
