# Ingredents
* 100% flour
* 35% warm water
* 31% full fat yogurt
* 7% melted butter
* 2.5% yeast
* 0.8% salt

# for 8 small naans
* 388g flour
* 135g warm water
* 120g full fat yogurt
* 27g melted butter
* 10g yeast
* 3g salt

# Method

* Bloom you yeast in the water.
* Add everything to the flour and mix untill it comes together a little.
* turn the dough out onto the counter and knead untill smooth (\~10 mins)
* create a tight ball and let rise untill doubled in size (\~ 1 hour)
* divide ball into 8 segments, roll them out into a mini naan shape, you want them \~3mm thick.
* let rise for 30-60 mins.
* heat a cast iron till it's ripping hot.
* lightly oil the cast iron and cook the naan on each side.
