# Ingredients
* 100% water
* 7.5% sugar (by volume)
* 10% lemon juice
* 4 tea bags per liter

# Method

* Bring water to boil, turn off the heat then desolve sugar.
* steap the teabags for 5 mins.
* add the lemon juice.
* let cool then rerigerate.
