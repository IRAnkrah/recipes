# Ingredients:
* 200g all purpose flour
* 300g sugar
  * 50g muscovado
  * 100g white
  * 150 demoreria
* 100g butter
* 1 egg
* 1 egg yoke
* 8g vanilla extract
* 3g salt
* 4g baking soda
* 150g dark chocolate

# Method:
* Mix all the sugars into a bowl and the flour, salt & baking soda in a second.
* Melt the butter (if you are feeling brave brown it) add the butter to the sugar, mix until a grainy paste forms.
* Add in order and fully mix between; egg, egg yoke, vanilla extract. By the end it should be a thick batter.
* Sift in ingredients of the second bowl and fold in (don't mix to aggressively or to much)
* Rough chop the chocolate and fold it in.

[optional]
Separate into portions (100g feels good)

* Seal the dough so that no surface is exposed to air and put in the fridge for 1 hour * 3 days.

[when you are going to cook]
* Preheat oven to 180c (350f), when ready shape dough into slight ovals and place vertical on a lined baking tin. until cooked.

# Notes
* when sealing I put the whole thing in an airtight container and then pressed cling film into the surface before putting the lid on.
* 2*3 days gave the best flavour/ texture in my opinion. 
* need to test the pan bang method (twice during cooking lift the plan slightly (~5cm) and drop this is meant to disrupt the rise and result in a chewer cookie)

# Refference
* <https://www.youtube.com/watch?v=ylxzfecackM>
* <https://www.youtube.com/watch?v=NYH1Z7TUSEI>
* <https://www.youtube.com/watch?v=wyuec0PPz68>
