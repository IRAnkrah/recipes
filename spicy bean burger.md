I have not made this recipe since I've started writing things down so it will be more a method that and recipe.

# Ingredents
* 250g red kidney beans (cooked)
* 125g chick peas (cooked)
* 2 carrots
* paprika
* curry powder
* hot sauce
* 1 egg
* 40g flour (this is made up you need a little flour to give it a some bite but I don't rememebr how much)
* breadcrumbs / crackers
* garlic

# Method
* add your beans to a bowl with the spices hot sauce and soy sauce then smush with your hands untill you have a mostly smooth paste with some bits in.
* finely grate your carrot in and combine.
* Taste and add seasionsings to your liking.
* add the egg and combine (it will be wet)
* now add the flour and combine.
* it should still be kinda wet but looking more like a dough now, so add breadcrumbs untill it comes together more and stops being sticky.
* leave it for 30 mins to fully hydrate and fry/grill under high temps.
