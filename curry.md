# ingredients
- 1-2 onion (i used 1 with you but would use two at home)
- 1/2 to full head of garlic ( used half at yours but would use a full at home if I have lots)
- about as much ginger by volume
- chickpeas _(either 2-3 cans or 300ml (by volume) of dry chickpeas)_
- 1 tin of tomato (crushed or blended till smooth)
- spices (one ramikin) _(i find it's pretty fiorgivnig and I just kinda throw stuff at it but these are the rough proptotions I think from memeory)_
	- curry powder 1/4
	- garam masala 1/4
	- cumin 1/4
	- chilie poweder 1/8
	- tumeric 1/8
	- you cand also put other things in if you like cloves, anasie (i don't like this one), paprika

# method
1. if you are using dry chick peas put them in the pressuere cooker with salted water (and a bay leaf if you want) with 2.5 times water up the sides of the pan and set pressure cooker for 45 mins (you will also let it natural relaise for 10 mins when it's done.)
2. heat the pan on medium heat (with no oil)
3. prepare all your ingedents cos the innitial cooking goes fast.
	1. put your spices in the ramikin so you can throw them in when it's time
	2. half your onion will be fried and half will be put in the paste with the half that will be fried slice it  with the half that will be in the paste cut into 1/8ths and put in the food processor.
	3. peal the garlic and cibe the ginger to garlic clove size and put both in the food processor. ginger and break it down to bits about the size of a garlic clove then put in the food processor
	4. put some oil and salt in the frood proccessor and blend till it's very broken down you (you may have to stop to scrape the sides a few times it's done when you not longer have to do that and it acts more like a liqud) if it's not braking down you may want to put some more oil or water in.
4. turn the pan up to high give it about a min then put enough oil in to just cover the bottom when the oil starts to shimmer put he sliced onions in. The goal it to get them soft but also develop some colour on htem so only stir oncaasionaly.
5. when the onions are done, turn the heat to medium high and put the spice mix in and stir constantly for a min or so 
6. now dump your paste in mix untlll you have everything combined let it fry off a bit stiring for a few mins till it tarts to look dryer.
7. turn the heat down to medium and add the tomato (and water if needed, you don't want it thick at this stage cos then you have to baby it while it's cooking) ans stir till combined
8. simmer with lid for 30 mins, then taste you will probably need to add salt and sugar at this stage with the sugar stir it in and give it 5 mins and another stir before you taste again.
9. you will want it to simmer for at least an hour but it can go for longer no probelm so long as there is enoug liquid.
10. if you are using dryedf chickpeas they should be done on the timer and have had their 10 min rest (more rest is not really a problem)
11. when you are 20 mins from serving strain the chickpeas and put in the curry (if you are using the pressure cooker for rice you can leave the chickpeas in the curry till the rice is done) 

# extras
- **Onion**: when cutting onion going pole to pole will give the slices more structural integrity so they stand up to cooking bettter and reatin some sembance of an onion which I prefer for this dish.
- **Garlic**: when i prepare garlic en mass I will cut the bottom off then lay the braod side of the knife on it and hit it with my palm a little that sslay makes it easier to peal.
- **Ginger**: when processing ginger you only have to tae the hard/dry bits off there is not need to peal it but if you want to I find the back of a knife or a spoon to be the best way to do that.
- **Curry** having the curry look a little wet is fine for most of hte cooking it will make it need less looking after when you want to thicen it up you can take off the lid and let it reduce that will not take more than 30 mins.